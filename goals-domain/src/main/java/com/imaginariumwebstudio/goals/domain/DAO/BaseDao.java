package com.imaginariumwebstudio.goals.domain.DAO;

import java.io.Serializable;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface BaseDao<T,I> {

    T byId(I id);

    Serializable save(T object);

    void update(T object);

    void remove(T object);

}