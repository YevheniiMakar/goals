package com.imaginariumwebstudio.goals.domain.querycreate;

public class OrderObject {

    private String columnName;
    /**
     * sortedType allowed values: ASC DESC
     */
    private String sortedType;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getSortedType() {
        return sortedType;
    }

    public void setSortedType(String sortedType) {
        this.sortedType = sortedType;
    }
}
