package com.imaginariumwebstudio.goals.domain.querycreate;

import java.util.List;

public class WhereObject {
    private String columnName;
    /**
     * whereOperator allowed values:
     * =, > , < , >= , <= , <> , BETWEEN , LIKE , IN
    */
    private String whereOperator;
    private List<String> parameters;
    /**
     * combiningOperator allowed values: AND, OR and NOT
     */
    private String combiningOperator;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getWhereOperator() {
        return whereOperator;
    }

    public void setWhereOperator(String whereOperator) {
        this.whereOperator = whereOperator;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public void setParameters(List<String> parameters) {
        this.parameters = parameters;
    }

    public String getCombiningOperator() {
        return combiningOperator;
    }

    public void setCombiningOperator(String combiningOperator) {
        this.combiningOperator = combiningOperator;
    }
}
