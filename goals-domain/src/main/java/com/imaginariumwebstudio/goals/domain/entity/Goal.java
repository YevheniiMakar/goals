package com.imaginariumwebstudio.goals.domain.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "goal")
@NamedQueries({
    @NamedQuery(name = "Goal.findAll", query = "SELECT g FROM Goal g")})
public class Goal implements Serializable {

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private User userId;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "time_type_goal")
    private String timeTypeGoal;
    
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;
    
    @Lob
    @Column(name = "goal_is_not_achieved")
    private String goalIsNotAchieved;
    
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    
    @Column(name = "execution_status")
    private String executionStatus;
    
    @Column(name = "re-execution")
    private String reExecution;

    public Goal() {
    }

    public Goal(Integer id) {
        this.id = id;
    }

    public Goal(Integer id, String timeTypeGoal, String name) {
        this.id = id;
        this.timeTypeGoal = timeTypeGoal;
        this.name = name;
    }

    public Goal(User userId, String timeTypeGoal, String name, String description, String goalIsNotAchieved,
                Date startDate, Date endDate, String executionStatus, String reExecution) {
        this.userId = userId;
        this.timeTypeGoal = timeTypeGoal;
        this.name = name;
        this.description = description;
        this.goalIsNotAchieved = goalIsNotAchieved;
        this.startDate = startDate;
        this.endDate = endDate;
        this.executionStatus = executionStatus;
        this.reExecution = reExecution;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTimeTypeGoal() {
        return timeTypeGoal;
    }

    public void setTimeTypeGoal(String timeTypeGoal) {
        this.timeTypeGoal = timeTypeGoal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGoalIsNotAchieved() {
        return goalIsNotAchieved;
    }

    public void setGoalIsNotAchieved(String goalIsNotAchieved) {
        this.goalIsNotAchieved = goalIsNotAchieved;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(String executionStatus) {
        this.executionStatus = executionStatus;
    }

    public String getReExecution() {
        return reExecution;
    }

    public void setReExecution(String reExecution) {
        this.reExecution = reExecution;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Goal)) {
            return false;
        }
        Goal other = (Goal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.imaginariumwebstudio.goals.domain.config.Goal[ id=" + id + " ]";
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

}
