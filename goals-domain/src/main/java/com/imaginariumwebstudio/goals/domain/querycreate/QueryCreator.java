package com.imaginariumwebstudio.goals.domain.querycreate;

import java.util.List;

public interface QueryCreator {

    String createWhere(List<WhereObject> whereObjectList);
    String createOrder(List<OrderObject> orderObjectList);
}
