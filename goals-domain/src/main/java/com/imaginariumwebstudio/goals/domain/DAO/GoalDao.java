package com.imaginariumwebstudio.goals.domain.DAO;

import com.imaginariumwebstudio.goals.domain.entity.Goal;
import com.imaginariumwebstudio.goals.domain.querycreate.OrderObject;
import com.imaginariumwebstudio.goals.domain.querycreate.WhereObject;

import java.util.List;

public interface GoalDao extends BaseDao<Goal, Integer>{
    
    List<Goal> getGoals();
    List<Goal> getGoals(List<WhereObject> whereObjectList, List<OrderObject> orderObjectList);

}
