package com.imaginariumwebstudio.goals.domain.DAO;

import com.imaginariumwebstudio.goals.domain.entity.User;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class UserDaoImpl extends AbstractDao<User, Integer> implements UserDao {
    @Override
    public User byUserName(String Name) {
        return null;
    }

    @Override
    public Serializable save(User object) {
        return null;
    }

    @Override
    public void update(User object) {

    }

    @Override
    public void remove(User object) {

    }
}
