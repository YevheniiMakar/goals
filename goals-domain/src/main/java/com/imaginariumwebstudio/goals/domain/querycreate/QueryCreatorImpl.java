package com.imaginariumwebstudio.goals.domain.querycreate;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Component
@Transactional
public class QueryCreatorImpl implements QueryCreator {
    @Override
    public String createWhere(List<WhereObject> whereObjectList) {

        if(whereObjectList.isEmpty()){
            return "";
        }

        StringBuilder where;

        where =new StringBuilder("WHERE ");
        int index = 0;
        for(WhereObject whereObject: whereObjectList){
            where.append(createBlockWhere(whereObject, index));
            index++;
        }

        return where.toString();
    }

    @Override
    public String createOrder(List<OrderObject> orderObjectList) {

        if(orderObjectList.isEmpty()){
            return "";
        }

        StringBuilder order =new StringBuilder("ORDER BY ");

        for(OrderObject orderObject: orderObjectList){
            order.append(orderObject.getColumnName()+" '" + orderObject.getSortedType() + "', ");
        }
        order.setLength(Math.max(order.length()-2,0));
        order.append(" ");

        return order.toString();
    }


    private StringBuilder createBlockWhere(WhereObject whereObject, int index){
        StringBuilder blockWhere = new StringBuilder();

        if(index !=0){
            blockWhere.append(whereObject.getCombiningOperator()+ " ");
        }

        blockWhere.append(whereObject.getColumnName()+" ");

        if (whereObject.getWhereOperator().equals("BETWEEN")){

            blockWhere.append(whereObject.getWhereOperator() + " '"
                                + whereObject.getParameters().get(0) + "' AND '"
                                + whereObject.getParameters().get(0)+"' ");

        } else if (whereObject.getWhereOperator().equals("IN")){
            blockWhere.append(whereObject.getWhereOperator() + " (");

            for (String parameter: whereObject.getParameters()){
                blockWhere.append("'"+parameter+"', ");
            }
            blockWhere.setLength(Math.max(blockWhere.length()-2,0));
            blockWhere.append(") ");

        } else {
            blockWhere.append(whereObject.getWhereOperator() + " '"
                    + whereObject.getParameters().get(0)+ "' ");


        }

        return blockWhere;

    }

}
