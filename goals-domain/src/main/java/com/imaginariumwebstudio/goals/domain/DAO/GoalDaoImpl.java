package com.imaginariumwebstudio.goals.domain.DAO;

import com.imaginariumwebstudio.goals.domain.entity.Goal;
import com.imaginariumwebstudio.goals.domain.querycreate.OrderObject;
import com.imaginariumwebstudio.goals.domain.querycreate.QueryCreator;
import com.imaginariumwebstudio.goals.domain.querycreate.WhereObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;


@Component
@Transactional
public class GoalDaoImpl extends AbstractDao<Goal, Integer> implements GoalDao {
    @Autowired
    QueryCreator queryCreator;


    @Override
    public List<Goal> getGoals() {
        return getCurrentSession().createSQLQuery("SELECT * FROM goal").addEntity(Goal.class).list();
    }

    @Override
    public List<Goal> getGoals(List<WhereObject> whereObjectList, List<OrderObject> orderObjectList) {
        return getCurrentSession().createSQLQuery(
                "SELECT * FROM goal "
                        + queryCreator.createWhere(whereObjectList)
                        + queryCreator.createOrder(orderObjectList))
                        .addEntity(Goal.class)
                        .list();
    }

}
