package com.imaginariumwebstudio.goals.domain.DAO;

import com.imaginariumwebstudio.goals.domain.entity.User;


public interface UserDao extends BaseDao<User, Integer> {
    
    User byUserName(String Name);
    
}
