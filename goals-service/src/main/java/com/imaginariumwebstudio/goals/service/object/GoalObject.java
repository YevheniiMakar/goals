package com.imaginariumwebstudio.goals.service.object;

import com.imaginariumwebstudio.goals.domain.entity.Goal;
import java.util.Date;




public class GoalObject {
      
    private static final long serialVersionUID = 1L;
    
    private Integer id;
        
    private String timeTypeGoal;
        
    private String name;
        
    private String description;
        
    private String goalIsNotAchieved;
        
    private Date startDate;
        
    private Date endDate;
        
    private String executionStatus;

    public GoalObject(String timeTypeGoal, String name, String description, String goalIsNotAchieved, Date startDate, Date endDate, String executionStatus, String reExecution, Integer userId) {
        this.timeTypeGoal = timeTypeGoal;
        this.name = name;
        this.description = description;
        this.goalIsNotAchieved = goalIsNotAchieved;
        this.startDate = startDate;
        this.endDate = endDate;
        this.executionStatus = executionStatus;
        this.reExecution = reExecution;
        this.userId = userId;
    }
    public GoalObject(Goal goal) {
        this.id = goal.getId();
        this.timeTypeGoal = goal.getTimeTypeGoal();
        this.name = goal.getName();
        this.description = goal.getDescription();
        this.goalIsNotAchieved = goal.getGoalIsNotAchieved();
        this.startDate = goal.getStartDate();
        this.endDate = goal.getEndDate();
        this.executionStatus = goal.getExecutionStatus();
        this.reExecution = goal.getReExecution();
        this.userId = goal.getUserId().getId();
        this.userId = goal.getUserId().getId();
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTimeTypeGoal() {
        return timeTypeGoal;
    }

    public void setTimeTypeGoal(String timeTypeGoal) {
        this.timeTypeGoal = timeTypeGoal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGoalIsNotAchieved() {
        return goalIsNotAchieved;
    }

    public void setGoalIsNotAchieved(String goalIsNotAchieved) {
        this.goalIsNotAchieved = goalIsNotAchieved;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(String executionStatus) {
        this.executionStatus = executionStatus;
    }

    public String getReExecution() {
        return reExecution;
    }

    public void setReExecution(String reExecution) {
        this.reExecution = reExecution;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
        
    private String reExecution;
    
    private Integer userId;
}
