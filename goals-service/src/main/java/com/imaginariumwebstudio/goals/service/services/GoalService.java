package com.imaginariumwebstudio.goals.service.services;

import com.imaginariumwebstudio.goals.service.object.GoalObject;

public interface GoalService extends BaseService<GoalObject, Integer> {

}
