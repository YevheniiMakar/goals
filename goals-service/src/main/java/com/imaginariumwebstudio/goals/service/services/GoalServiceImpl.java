package com.imaginariumwebstudio.goals.service.services;

import com.imaginariumwebstudio.goals.domain.DAO.GoalDao;
import com.imaginariumwebstudio.goals.domain.DAO.UserDao;
import com.imaginariumwebstudio.goals.domain.entity.Goal;
import com.imaginariumwebstudio.goals.domain.entity.User;
import com.imaginariumwebstudio.goals.service.object.GoalObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;

@Transactional
@Component
public class GoalServiceImpl implements GoalService{

    @Autowired
    private GoalDao goalDao;
    @Autowired
    private UserDao userDao;

    @Override
    public GoalObject byId(Integer id) {
        return new GoalObject(goalDao.byId(id));
    }

    @Override
    public Serializable save(GoalObject goalObject) {
        goalDao.save(new Goal(userDao.byId(goalObject.getUserId()), goalObject.getTimeTypeGoal(), goalObject.getName(),
                goalObject.getDescription(), goalObject.getGoalIsNotAchieved(),
                goalObject.getStartDate(), goalObject.getEndDate(), goalObject.getExecutionStatus(), goalObject.getReExecution()));
        return null;
    }

    @Override
    public void update(GoalObject object) {

    }

    @Override
    public void delete(GoalObject object) {

    }
}
