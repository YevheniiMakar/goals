package com.imaginariumwebstudio.goals.service.services;

import java.io.Serializable;


public interface BaseService<T, I> {
    
    T byId(I id);

    Serializable save(T object);

    void update(T object);

    void delete(T object);

}
